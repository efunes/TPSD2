/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.pol;

import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;

/**
 *
 * @author Guille
 */
@WebService(serviceName = "WsBank")
public class WsBank {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "depositar")
    //@Oneway
    public void depositar(@WebParam(name = "id") int id, @WebParam(name = "monto") double monto)throws Exception {
        EntityManager em = PersistenceUtil.getEntityManager();
        Cuentas cuenta = this.obtener(id);
        try {
            // se inicia la transacción
            em.getTransaction().begin();
            // se verifica si el cliente ya no existe en la BD

            if (cuenta.getIdCuenta() == null) {
                // se persiste el cliente si este no existe
                cuenta.setIdCuenta(id);
                cuenta.setMonto(monto);
                em.persist(cuenta);
            } else {
                // se actualiza el cliente si existe
                cuenta.setMonto(cuenta.getMonto() + monto);
                em.merge(cuenta);
            }
            // se confirma la transacción
            em.getTransaction().commit();
        } catch (Exception e) {
            // si ocurre un error entonces se deshace la transacción
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            // se lanza un error indicando que no se puede guardar
            // se asocia a la excepcion lanzada la excepcion capturada
            throw new Exception("No se ha podido realizar el deposito en la cuenta : "
                    + cuenta.toString(), e);
        } finally {
            em.close();
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "obtener")
    public Cuentas obtener(@WebParam(name = "codigo") int codigo) {
       EntityManager em = PersistenceUtil.getEntityManager();
       Cuentas cuenta = em.find(Cuentas.class, codigo);
       em.close();
       return cuenta;
    }
    
    
    
}
