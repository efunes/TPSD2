/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.pol;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Implementa la clase de a traves de la cual se obtienen Objetos Entity
 * Managers capaces de manejar la persistencia de objetos.
 * @author gustavo
 */
public class PersistenceUtil {

    /*
     * Todo atributo estatico se crea al momento en que la máquina virtual
     * carga la clase, de manera que cuando se inicia el sistema se crea
     * el entity manager factory que se conecta con la BD
     */
    private static EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("Core_Banco");

    /**
     * retorna un objeto Entity Maneger para manejar la persistencia de los
     * objetos.
     */
    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
}
