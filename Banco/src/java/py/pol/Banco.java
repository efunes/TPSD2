/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.pol;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.db.*;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RANGE
 */
@WebService(serviceName = "Banco")
public class Banco {
    private String errorPrefix = "Error: ";
    /**
     * Web service operation
     */    
    @WebMethod(operationName = "AddUser")
    public String AddUser(@WebParam(name = "Id") int Id, @WebParam(name = "Nombre") String Nombre, @WebParam(name = "Apellido") String Apellido) {
        String ret = "0";
        Cuenta a = new Cuenta();
        try {
            EntityManagerFactory emd= Persistence.createEntityManagerFactory("RANGE");ret="1";
            ControladorCuenta controlador = new ControladorCuenta(emd);ret="2";
            a.setId(Id);a.setNombre(Nombre);a.setApellido(Apellido);a.setSaldo(100000.0);ret="3";
            controlador.edit(a);ret="4";
        } catch (Exception ex) {
            ret= errorPrefix+ret;            
        }
        return ret;
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "DeleteUser")
    public String DeleteUser(@WebParam(name = "Id") int Id) {
        String ret = "0";
        Cuenta a = new Cuenta();
        try {
            EntityManagerFactory emd= Persistence.createEntityManagerFactory("RANGE");ret="1";
            ControladorCuenta controlador = new ControladorCuenta(emd);ret="2";
            if(controlador.findCuenta(Id)==null){
                ret="3";
            }else{
                ret="4";
            }
            controlador.destroy(Id);ret="Borrado";            
        } catch (Exception ex) {
            ret= errorPrefix+ret;
        }
        return ret;
    }
    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "depositar")
    public int depositar(@WebParam(name = "id") int id, @WebParam(name = "monto") double monto) {
        int ola=-1;
        try {
            EntityManagerFactory emd= Persistence.createEntityManagerFactory("RANGE");
            ControladorCuenta ctrl =new ControladorCuenta(emd);
            ola = 0;
            Cuenta cuenta=ctrl.findCuenta(id);
            Cuenta cuentabanco =ctrl.findCuenta(1010);
            if(ctrl.findCuenta(id)==null){
                ola=1;
                cuenta.setId(id);
                cuenta.setSaldo(monto);
                ctrl.create(cuenta);
            }else{
                double saldo = cuenta.getSaldo();
                double saldobanco=cuentabanco.getSaldo();
                
                if(saldobanco>monto){
                    saldobanco-=monto;
                    cuentabanco.setSaldo(saldobanco);
                    ctrl.edit(cuentabanco);
                    
                    saldo+=monto;
                    cuenta.setSaldo(saldo);
                    ctrl.edit(cuenta);
                    
                }else{
                    ola=2;
                }
               
            }   
        } catch (Exception ex) {
            //ret= "Se ha producido un error";
            
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ola;
        // 0 si no hay errores
        // 1 el id no esta registrado
        // 2 el saldo del banco es menor a lo que se quiere extraer
    }
    public int extraccion(@WebParam(name = "id") int id, @WebParam(name = "monto") double monto) {
        int ola=0;
        try {
            EntityManagerFactory emd= Persistence.createEntityManagerFactory("RANGE");ola = 0;
            ControladorCuenta ctrl =new ControladorCuenta(emd);
            Cuenta cuenta = ctrl.findCuenta(id);
            Cuenta cuentabanco = ctrl.findCuenta(1010);
            if(ctrl.findCuenta(id)==null){
                ola=1;
                cuenta.setId(id);
                cuenta.setSaldo(monto);
                ctrl.create(cuenta);
            }else{
                double saldo = cuenta.getSaldo();
                double saldobanco=cuentabanco.getSaldo();
                if(saldo>monto){
                    saldo-=monto;
                    cuenta.setSaldo(saldo);
                    ctrl.edit(cuenta);
                    
                    saldobanco+=monto;
                    cuentabanco.setSaldo(saldobanco);
                    ctrl.edit(cuentabanco);
                }else{
                    ola=2;
                }
               
            }   
            } catch (Exception ex) {
                //ret= "Se ha producido un error";
                Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            }
        return ola;
        // 0 si todo esta ok
        // 1 si el id no esta registrado
        // 2 si el saldo del usuario es menor a lo que se quiere extraer
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "transaccion")
    public int transaccion(@WebParam(name = "id_origen") int id_origen, @WebParam(name = "id_destino") int id_destino, @WebParam(name = "monto") double monto) {
        int i=0;
        if(extraccion(id_origen,monto)==0){
            i=depositar(id_destino,monto);
        }else{
            i=3;        
        }
        if(i!=0 && i!=3)depositar(id_origen,monto);         //solo entra en caso de que el deposito halla fallado
                                                            //no debe entrar en caso de que la extraccoin falle
        return i;
        //retorna 0 si la transaccion fue exitosa
        //retorna distinto de cero si la transaccion no fue exitosa 
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "transaccion_multimoneda")
    public double transaccion_multimoneda(@WebParam(name = "id_origen") int id_origen, @WebParam(name = "id_destino") int id_destino, @WebParam(name = "monto") double monto, @WebParam(name = "tipo") String tipo) {
        String msg=consulta(tipo);
        int  valor_cambio=Integer.parseInt(msg);
        monto=valor_cambio*monto;
        return transaccion(id_origen,id_destino,monto);
        
    }
    private static String consulta(java.lang.String moneda) {
        py.pol.ExchangeHouse service = new py.pol.ExchangeHouse();
        py.pol.Exchange port = service.getExchangePort();
        return port.consulta(moneda);
    }
}
