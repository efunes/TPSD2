/* JNDISD
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.pol;
import py.db.*;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 *
 * @author RANGE
 */
@WebService(serviceName = "ExchangeHouse")
public class Exchange {
    private String errorPrefix = "Error: ";

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consulta")
    public String consulta(@WebParam(name = "Moneda") String Moneda) {
        String ret = "0";
        py.db.Exchange a;
        try {
            EntityManagerFactory emd= Persistence.createEntityManagerFactory("RANGE");ret="1";
            ControladorExchange controlador = new ControladorExchange(emd);ret="2";
            a=controlador.findExchange(Moneda);ret="3";
            ret= a.getVenta()+"";
        } catch (Exception ex) {
            ret= errorPrefix+ret;            
        }
        return ret;
    }

}
