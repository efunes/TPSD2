/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.db;

import java.io.Serializable;
import java.util.List;
import py.db.exceptions.NonexistentEntityException;
import py.db.exceptions.PreexistingEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author PC-14
 */
public class ControladorExchange implements Serializable {

    public ControladorExchange(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Exchange exchange) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(exchange);
            em.getTransaction().commit();
        } catch (Exception ex) {
            /*if (findExchange(exchange.getMoneda()) != null) {
                throw new PreexistingEntityException("Exchange " + exchange + " already exists.", ex);
            }
            throw ex;*/
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Exchange exchange) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            exchange = em.merge(exchange);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = exchange.getMoneda();
                if (findExchange(id) == null) {
                    throw new NonexistentEntityException("The exchange with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Exchange exchange;
            try {
                exchange = em.getReference(Exchange.class, id);
                exchange.getMoneda();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The exchange with id " + id + " no longer exists.", enfe);
            }
            em.remove(exchange);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Exchange> findExchangeEntities() {
        return findExchangeEntities(true, -1, -1);
    }

    public List<Exchange> findExchangeEntities(int maxResults, int firstResult) {
        return findExchangeEntities(false, maxResults, firstResult);
    }

    private List<Exchange> findExchangeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Exchange.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Exchange findExchange(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Exchange.class, id);
        } finally {
            em.close();
        }
    }

    public int getExchangeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Exchange> rt = cq.from(Exchange.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
